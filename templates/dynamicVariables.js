var dynamicVariable =
{
    currentUserID: {{ current_user.id }},
    firstNode: {{ active_node.to_vis() | json if active_node else '-1' }},
    feedMinDate: '{{ feed.min_date if feed else 'None' }}',
    feedMaxDate: '{{ feed.max_date if feed else 'None' }}',
    token: '{{ csrf_token() }}'
}
