import json
from app import app, db, node_manager, login_manager
from flask import request, flash, url_for, redirect, abort, render_template as template, jsonify
from flask_login import login_user, logout_user, current_user, login_required
from models import User
from input_interpreter import UserInputInterpreter, UserInputException, UserInputError
from completer import ThoughtCompleter
from completer import SearchCompleter
from neo4jrestclient.exceptions import NotFoundError
from user_status import StatusManager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('browser'))
    else:

        data = {
            'starting': get_random_nodes(),
        }
        return template('welcome.html', **data)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        if request.args.get('embedded'):
            return template('login-embedded.html')
        else:
            return template('login.html')
    email = request.form['email']
    password = request.form['password']
    remember_me = 'remember_me' in request.form

    user = User.query.filter_by(email=email).first()
    if user is None:
        flash('Email is invalid', 'error')
        return redirect(url_for('login'))

    if user.authenticate(password):
        login_user(user, remember=remember_me)
        # flash('Logged in successfully')
        return redirect(request.args.get('next') or url_for('index'))
    else:
        flash('Password is invalid', 'error')
        return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method != 'POST':
        return redirect(url_for('index'))

    password = request.form['password']
    email = request.form['email']
    confirm_email = request.form['confirm_email']

    if password and email and (email == confirm_email):
        is_there_user_with_that_mail = User.query.filter_by(email=email).count()
        if is_there_user_with_that_mail:
            flash('This email is already used :(')
        else:
            user = User(password, email)
            db.session.add(user)
            db.session.commit()
            login_user(user)
    else:
        flash('Password and email have to be non-empty!')

    return redirect(request.args.get('next') or url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/add_edge', methods=['POST'])
@login_required
def add_edge():

    data = request.get_json()

    node_1 = node_manager.get(data['from'])
    node_2 = node_manager.get(data['to'])

    if node_manager.is_visible(node_1) and node_manager.is_visible(node_2):

        edge = node_manager.add_edge(node_1, node_2, '_dragged', 'public')
        response = edge.to_vis()

        status = StatusManager(node_manager)

        status_data = {
            'edges': [edge],
            'visibility': edge.visibility,
            'action': 'add',
            'obj_type': 'edge'
        }

        # update status of user
        status.push(status_data)

    else:
        abort(401)

    response['vis_id'] = data['vis_id']

    return jsonify(**response)


@app.route('/add_node', methods=['POST'])
@login_required
def add_node():

    response = {'success': 0}

    try:

        user_input_interpreter = UserInputInterpreter(node_manager)

        result = user_input_interpreter.new_nodes(request, current_user)

        response['new_nodes'] = [node.to_vis() for node in result['new_nodes']]
        response['new_edges'] = [edge.to_vis() for edge in result['new_edges']]

        response['success'] = result['success']
        response['type'] = result['type']

    except UserInputException as e:
        message = e.get_message()
        response['error'] = message
        app.logger.warning(message)

    except UserInputError as e:
        message = e.get_message()
        response['error'] = message
        app.logger.error(message)

    return jsonify(**response)


@app.route('/complete')
@login_required
def complete():
    query = request.args.get('q')
    present = request.args.get('p')

    thought_completer = ThoughtCompleter(node_manager)
    data = thought_completer.complete(query, present)

    return json.dumps(data)


@app.route('/search')
def search():
    query = request.args.get('q')

    search_completer = SearchCompleter(node_manager)
    data = search_completer.complete(query)

    return json.dumps(data)


@app.route('/remove_node', methods=['POST'])
@login_required
def remove_node():
    data = request.get_json()

    node_id = data['id']

    node = node_manager.get(node_id)

    if node.owner == current_user.id:

        success = node_manager.remove(node)

        data = {
            'id': node_id,
            'success': success
        }

        return jsonify(**data)
    else:
        abort(401)
        return ''


@app.route('/change_node_label', methods=['POST'])
@login_required
def change_node_label():
    data = request.form

    node_id = data['id']
    label = data['label']

    node = node_manager.get(node_id)

    if node.owner == current_user.id:
        assert label
        node.set('name', label)

        data = {
            'id': node_id,
            'label': label
        }

        status = StatusManager(node_manager)

        status_data = {
            'nodes': [node],
            'visibility': node.visibility,
            'action': 'edit',
            'obj_type': node.type
        }

        # update status of user
        status.push(status_data)

        return jsonify(**data)
    else:
        abort(401)
        return ''


@app.route('/get_node')
@login_required
def get_node():
    # get node data
    node_id = request.args.get('id')

    try:
        node = node_manager.get(node_id)

        # is user allowed to see this?

        if node_manager.is_visible(node):
            # save that the user was interested in the node
            # current_user.history.add(node_id) # TODO - in relational or graph?

            # create response
            data = {
                'html': template('active_node_details.html',
                                 active_node=node),
                'node': node.to_vis(),
                'success': True
            }
        else:
            error = 'You are not allowed to see this node'
            data = {
                'html': error,
                'node': {'id': node_id, 'owner': -1, 'label': error}
            }

    except NotFoundError:
        error = 'Node does not exists'
        data = {
            'html': error,
            'node': {'id': node_id, 'owner': -1, 'label': error}
        }

    # pack them to json and send to browser
    return jsonify(**data)


def data_to_vis(nodes):
    edges = node_manager.get_edges_between(nodes)

    nodes_data = [node.to_vis() for node in nodes]
    edges_data = [edge.to_vis() for edge in edges]

    data = {
        'nodes': json.dumps(nodes_data),
        'edges': json.dumps(edges_data)
    }

    return data


def get_random_nodes():

    nodes = node_manager.get_random()

    return data_to_vis(nodes)


@app.route('/get_nodes_around')
@login_required
def get_nodes_around(node_id=None):

    if not node_id:
        node_id = request.args.get('id')

    assert node_id

    # get nodes that are visible for user
    nodes = node_manager.get_around(node_id)

    return data_to_vis(nodes)


def get_starting_nodes(node_id=None):

    # TODO - this is here only for easier work on this etap of developments:wq
    return data_to_vis(node_manager.get_all())

    # TODO starting nodes should include always:
    # 1. user node
    # 2. sth trendy
    # 3. sth last used
    # 4. sth unconnected to user's network
    # and we need some other function here in future

    # if not node_id -> get skip 'around'
    nodes = [node_manager.get(node_id)]
    nodes += node_manager.get_around(node_id)

    return data_to_vis(nodes)


@app.route('/news_feed')
def news_feed():

    status = StatusManager(node_manager)

    feed = status.get_feed(**request.args.to_dict())

    if feed.length():
        html = template('news.html', feed=feed)

        response = {
            'html': html,
            'min': feed.min_date,
            'max': feed.max_date,
            'end': feed.end
        }
    else:
        response = {'html': ''}

    return jsonify(**response)


@app.route('/node')
@login_required
def node_view():
    # TODO check rights, add stats like above
    node_id = request.args.get('id')

    try:
        node = node_manager.get(node_id)

        if not node_manager.is_visible(node):
            flash('You are not allowed to see this node')
            return redirect(url_for('browser'))

        status = StatusManager(node_manager)

        data = {
            'active_node': node,
            'starting': get_starting_nodes(node_id),
            'feed': status.get_feed()
        }
    # TODO: use own exception with message from node-manager instead of proxy's one.
    except NotFoundError:
        flash('The node does not exists')
        return redirect(url_for('browser'))

    return template('browser.html', **data)


@app.route('/explore')
@login_required
def browser():

    status = StatusManager(node_manager)

    data = {
        'starting': get_starting_nodes(current_user.get_id()),
        'feed': status.get_feed()
    }
    return template('browser.html', **data)


@app.route('/license')
def license():

    licenses = [
        {
            'full_name': 'The MIT License (MIT)',
            'shortcut': '',
            'name': 'MIT',
            'url': 'http://opensource.org/licenses/MIT',
            'components':
            [
                {
                    'name': 'vis.js',
                    'url': 'http://visjs.org/',
                    'desc': 'A dynamic, browser based visualization library. The library is designed to be easy to use, to handle large amounts of dynamic data, and to enable manipulation of and interaction with the data',
                    'authors': ['Almende B.V']
                },
                {
                    'name': 'Bootstrap 3',
                    'desc': 'Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.',
                    'url': 'http://getbootstrap.com/',
                },

            ]
        },
        {
            'full_name': 'Apache License, Version 2.0',
            'name': 'Apache 2',
            'url': 'http://www.apache.org/licenses/LICENSE-2.0',
            'components':
            [
                {
                    'name': 'Selectize.js',
                    'desc': 'Selectize is the hybrid of a textbox and <select> box. It is jQuery-based and it is useful for tagging, contact lists, country selectors, and so on.',
                    'url': 'http://selectize.github.io/selectize.js/',
                    'authors': ['Brian Reavis']
                }
            ]
        }
    ]

    data = {
        'licenses': licenses
    }
    return template('license.html', **data)
