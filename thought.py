from app import node_manager


class Thought(object):

	def __init__(self, root_node):
		self.root = root_node

	def get_subtree(self, root):

		if len(root.edges) == 0:
			return []

		list = []

		for edge_group_role, edge_group in root.edges.items():
			if edge_group_role.startswith('_'):
				continue
			for edge in edge_group:
				if edge.start == root.id:
					# not this direction
					continue
				from_node = node_manager.get(edge.start)
				list += [self.get_subtree(from_node) + [(edge_group_role, edge.start)]]

		return list

	def ids_from_tree(self, tree):
		list = []

		if type(tree) == tuple:
			return [tree[1]]

		for group in tree:
			list += self.ids_from_tree(group)
		return list

	def __str__(self):
		tree = self.get_subtree(self.root)
		for i in xrange(len(tree)):
			if tree[i][0][0] == 'nsubj':
				tree = tree[:i] + [[('root', self.root.id)]] + tree[i:]
				break

		sent_ids = self.ids_from_tree(tree)[::-1]

		sent_nodes = [node_manager.get(i) for i in sent_ids]

		sentence = ' '.join([node.name for node in sent_nodes])

		return str(sentence)
