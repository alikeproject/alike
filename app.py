from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from neo4jrestclient.client import GraphDatabase
from flask_login import LoginManager
from node_manager import NodeManager
from flask_assets import Environment, Bundle
from flask_htmlmin import HTMLMIN

app = Flask(__name__)
app.config.from_pyfile('config.py')

"""
Connect databases, Login Manager
"""


db = SQLAlchemy(app)

graph = GraphDatabase(app.config['GRAPH_DATABASE'], app.config['GRAPH_USER'], app.config['GRAPH_PASSWORD'])

node_manager = NodeManager(graph, app.config['NODE_TYPES'])

"""
Connect plugins
"""

from models import AnonymousUser

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.anonymous_user = AnonymousUser

html_min = HTMLMIN()
# html_min.init_app(app)

"""
Define assets
"""

assets = Environment(app)

js_browser = Bundle('js/browser_modules/*.js',
                    'js/browser.js',
                    filters='rjsmin',
                    output='browser.min.js')

js_network = Bundle('js/network_modules/*.js',
                    'js/network.js',
                    filters='rjsmin',
                    output='network.min.js')

css_common = Bundle('style.css',
                    filters='cssutils',
                    output='style.min.css')

assets.register('js_browser', js_browser)
assets.register('js_network', js_network)
assets.register('css_common', css_common)


"""
Register functions for Jinja
"""

import file_manager
import json
import date_time
import thought
import csrf

app.jinja_env.lstrip_blocks = True

app.jinja_env.globals['csrf_token'] = csrf.new_csrf_token
app.jinja_env.globals['accepted_mimetypes'] = file_manager.accepted_mimetypes
app.jinja_env.globals['get_node'] = node_manager.get
app.jinja_env.globals['date_time'] = date_time.DateTime
app.jinja_env.globals['thought'] = thought.Thought

app.jinja_env.filters['json'] = json.dumps

"""
Import viwes
"""

import views
