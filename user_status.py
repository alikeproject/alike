from flask_login import current_user
from neo4jrestclient import client
from neo4jrestclient.exceptions import NotFoundError
from node_manager.node import Node
from datetime import datetime
from copy import copy


class News(object):

    def __init__(self, node_manager, status_node, author_node):
        self.nm = node_manager
        self.status = Node(status_node)
        self.author = Node(author_node)

    def check_integrity(self):
        if self.object == 'edge':
            to_check = self.status._status_edges
            checker = self.nm.get_edge
        else:
            to_check = self.status._status_nodes
            checker = self.nm.get

        for obj_id in to_check.split(','):
            try:
                checker(obj_id)
            except NotFoundError:
                self.status.set('skip', True)
                return False
        return True

    def __getattr__(self, attr):
        try:
            return copy(self.status.__getattr__(attr))
        except KeyError:
            raise AttributeError

    def get_node(self, nr):
        node_id = self.status._status_nodes.split(',')[nr]
        return self.nm.get(node_id)

    def get_edge(self, nr):
        edge_id = self.status._status_edges.split(',')[nr]
        return self.nm.get_edge(edge_id)

    @property
    def start(self):
        node_id = self.get_edge(0).start
        return self.nm.get(node_id)

    @property
    def end(self):
        node_id = self.get_edge(0).end
        return self.nm.get(node_id)

    @property
    def object(self):
        return self.status.obj_type

    def determinant(self, object):
        owner = self.describe_ownership(object)
        if owner:
            return owner
        return 'the'
        """
        if object.name[0].lower() in 'aeiou':
            return 'an'
        return 'a'
        """

    def describe_ownership(self, object, on_undefined=''):
        owner = object.owner
        author = self.author.id

        if not author == current_user.id:
            if owner == current_user.id:
                return 'your'
        if owner == author:
            # TODO determine sex
            #  if cypher w stylu (a:user)-(:action{name:is}..0*3)->(b{name:male})
            # elif --//-- name:female
            # return 'his/her'
            return 'his'

        return on_undefined


class Feed(object):

    def __init__(self, news_list, end):
        self.news_list = news_list
        self.end = end

    def __iter__(self):
        for news in self.news_list:
            yield news

    def length(self):
        return len(self.news_list)

    @property
    def max_date(self):
        max_date = str(datetime.min)
        for news in self.news_list:
            max_date = max([max_date, news.date])
        return max_date

    @property
    def min_date(self):
        min_date = str(datetime.max)
        for news in self.news_list:
            min_date = min([min_date, news.date])
        return min_date


class StatusManager(object):

    def __init__(self, node_manager):
        self.nm = node_manager

    def get_feed(self, max_posts=10, look_in_history_until=100, date_from=datetime.min, date_to=datetime.max):

        assert int(max_posts) < 1000

        query = """
        MATCH (n:user)-[rels]-(friend:user)
        WHERE id(n) = {user}
        WITH friend
        MATCH (friend)-[:_status]-(latestupdate)-[:_next*0..{max_per_user}]-(statusupdates)
        WHERE '{date_from}' < statusupdates.date AND statusupdates.date < '{date_to}' AND NOT exists(statusupdates.skip)
        RETURN DISTINCT statusupdates, friend
        ORDER BY statusupdates.date DESC LIMIT {max}
        """.format(user=current_user.id,
                   max_per_user=look_in_history_until - 1,
                   max=max_posts,
                   date_from=str(date_from),
                   date_to=str(date_to))

        results = self.nm.custom_query(query, returns=(client.Node, client.Node))

        news_list = [News(self.nm, *result) for result in results]

        # Following method is relatively prone to errors.
        # If I have 'len(news_list) == max_posts' and only 'max_posts' that fits
        # my query, there is '1/max_posts' chance that I'm wrong.
        # Practically, in most cases this will be correct - and is really cheap!
        end = len(news_list) != max_posts

        return Feed(filter(News.check_integrity, news_list), end)

    def get_last_status(self):
        pass

    def push(self, status, user=None):
        if user is None:
            user = current_user

        from datetime import datetime
        now = datetime.utcnow()

        for elements_key in ['nodes', 'edges']:
            if elements_key in status:
                status['_status_' + elements_key] = ','.join([str(e.id) for e in status.pop(elements_key)])

        args = []
        for key, value in status.items():
            if type(value) == unicode:
                value = str(value)
            if value == '':
                continue
            args.append(key + ': ' + repr(value))

        query = """
        MATCH (n:user)
        WHERE id(n) = {user}
        OPTIONAL MATCH (n)-[r:_status]-(old_status:status)
        DELETE r
        CREATE (n)-[:_status {{ owner: {user_id} }} ]->(new_status:status {{ name: '_', date: '{now}', owner: {user_id}, {args} }})
        WITH new_status, collect(old_status) AS old_statuses
        FOREACH (x IN old_statuses | CREATE (new_status)-[:_next]->(x))
        """.format(user=user.id,
                   args=', '.join(args),
                   now=now,
                   user_id=user.id)

        new_status = self.nm.custom_query(query)

        return new_status
