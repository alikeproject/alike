from app import db, graph

print "DB reset:"

success = True

print "Removing all nodes and edges from graph database..."

query = """
MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n,r
"""

try:
    graph.query(query)
except Exception:
    success = False
    print "Cleaning graph database failed."

print "Removing relational database..."

try:
    # http://jrheard.tumblr.com/post/12759432733/dropping-all-tables-on-postgres-using
    db.reflect()
    db.drop_all()
except Exception:
    success = False
    print "Removing relational database failed."

print "Recreating relational database..."


try:
    db.create_all()
except Exception:
    success = False
    print "Recreating relational database failed."

if success:
    print "Done, databases reset successfully."
else:
    print "Resetting databases failed."
