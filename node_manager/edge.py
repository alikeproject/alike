from copy import copy


class Edge(object):

    def __init__(self, edge):
        self._edge = edge

    def __getattr__(self, attr):
        try:
            return copy(self._edge.properties[attr])
        except KeyError:
            raise AttributeError

    def to_vis(self):
        data = {'from': self.start,
                'to': self.end}

        # TODO lists should be somewhere else
        # TODO list of types where we don't wont arrows
        if self.type in []:
            data['arrows'] = {}

        if self.type not in [] and not self.type.startswith('_'):
            data['label'] = self.type

        return data

    @property
    def start(self):
        return self._edge.start.id

    @property
    def end(self):
        return self._edge.end.id

    @property
    def type(self):
        return self._edge.type

    @property
    def id(self):
        return self._edge.id
