from copy import copy
from edge import Edge

NODE_NAMING_RULES = {
    '.': '_',   # reserved as sentence stop
    # ' ': '_',   # reserved as separator
    # "'": '_',   # reserved as separator
    '|': '_',   # reserved as selectize separator
    '#': '_'    # reserved for node ids
}


def encode_name(name):
    for k, v in NODE_NAMING_RULES.iteritems():
        name = name.replace(k, v)
    return name


class Node(object):

    def __init__(self, node):
        self._node = node

    def __getattr__(self, attr):
        try:
            return copy(self._node.properties[attr])
        except KeyError:
            raise AttributeError

    @property
    def type(self):
        assert len(self._node.labels) == 1
        # (label - as in no4j database)
        return list(self._node.labels).pop()._label

    @property
    def id(self):
        return self._node.id

    @property
    def raw(self):
        return self._node

    def set(self, key, value):
        self._node[key] = value

    def to_vis(self):

        data = {
            'label': self.name,
            'id': self.id,
            'type': self.type,
            'owner': self.owner     # for enabling editing
        }

        if self.type == 'picture':
            data['image'] = self.src
            data['shape'] = 'circularImage'

        return data

    @property
    def has_edges(self):
        return bool(self.raw.relationships.all())

    @property
    def edges(self):
        edge_list = [Edge(edge) for edge in self.raw.relationships.all()]

        edge_dict = {}

        for edge in edge_list:
            if edge.type not in edge_dict:
                edge_dict[edge.type] = [edge]
            else:
                edge_dict[edge.type] += [edge]

        return edge_dict
