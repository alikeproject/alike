from neo4jrestclient import client
from flask_login import current_user
from natural_language import NaturalLanguageError
from datetime import datetime
from edge import Edge
from node import Node
import node


nlp = None


class Visibility(object):

    def __init__(self, visibility_string):

        self.visibility = visibility_string

    def is_correct(self):

        try:
            self.get_query()
            return True
        except NaturalLanguageError:
            return False

    def get_query(self, node=''):

        if self.visibility == 'public':
            # match all users

            query = """
            MATCH (n:user)
            WHERE id(n) = {current_user}
            RETURN n;
            """
        elif self.visibility == 'private':
            # match current user

            query = """
            MATCH (n:user)
            WHERE id(n) = {node_owner} AND id(n) = {current_user}
            RETURN n;
            """
        else:
            # parse
            query = nlp.to_query(self.visibility)

        if node:
            query = query.format(
                current_user=current_user.id,
                node_owner=node.owner)

        return query


class NodeManager(object):

    def __init__(self, graph_database, types=None):
        self.graph = graph_database
        if types:
            self.node_types = types
        else:
            self.node_types = []

    def add(self, node_type, node_name, visibility, **kwargs):

        assert node_type in self.node_types
        assert Visibility(visibility).is_correct()

        node_name = node.encode_name(node_name)

        now = datetime.utcnow()

        new_node = self.graph.node(name=node_name, visibility=visibility, date=now, **kwargs)
        new_node.labels.add(node_type)

        # It is not given as argument at purpose:
        # We have to new node's id if it is created from unauthenticated user
        if current_user.is_authenticated:
            owner_id = current_user.id
        else:
            owner_id = new_node.id

        new_node['owner'] = owner_id

        return Node(new_node)

    def add_edge(self, node_start, node_end, edge_type, visibility, **kwargs):

        owner_id = current_user.id
        now = datetime.utcnow()

        edge = node_start.raw.relationships.create(edge_type, node_end.raw, visibility=visibility, date=now, owner=owner_id)

        return Edge(edge)

    def get(self, node_id):
        """
        Always returns node of given id, also when it is invisible for user
        :param node_id:
        :return: Node object
        """
        # in for strings use: filter(map(str.isdigit, node_id))
        assert type(node_id) == int or node_id.isdecimal()

        node = self.graph.nodes.get(node_id)

        return Node(node)

    def get_edge(self, edge_id):

        assert type(edge_id) == int or edge_id.isdecimal()

        edge = self.graph.relationships.get(edge_id)

        return Edge(edge)

    def query(self, query):

        nodes = self.system_query(query)

        return filter(self.is_visible, nodes)

    def custom_query(self, query, returns=None):

        return self.graph.query(query, returns=returns)

    def system_query(self, query):

        nodes = []

        results = self.graph.query(query, returns=client.Node)

        for result in results:
            nodes += [Node(raw) for raw in result]

        return nodes

    def query_edges(self, query):

        edges = []

        results = self.graph.query(query, returns=client.Relationship)

        for result in results:
            edges += [Edge(raw) for raw in result]

        return edges

    def get_random(self, limit=15):

        query = """
        MATCH (n)
        RETURN n
        LIMIT {limit}
        """.format(limit=limit)

        return self.query(query)

    def get_all(self):

        query = """
        MATCH (n)
        RETURN n;
        """

        return self.query(query)

    def get_by_type(self, node_type):

        assert node_type in self.node_types

        type_obj = self.graph.labels.create(node_type)

        # TODO nietestowane
        return type_obj.get_all()

    def get_around(self, node_id):

        query = """
        MATCH (x)-[*1..3]->(y)
        WHERE id(x) = {node_id} AND NOT x = y
        RETURN DISTINCT y
        """.format(node_id=node_id)

        return self.query(query)

    def remove(self, node):

        # You can not remove user node with public API - security issues
        if node.type == 'user':
            return False

        query = """
        MATCH (n:{node_type})
        WHERE id(n) = {node_id}
        DETACH DELETE n
        """.format(node_id=node.id, node_type=node.type)

        return self.system_query(query)

    def is_visible(self, node):

        # System nodes starts from '_'. They should not be visible.
        if node.name.startswith('_'):
            return False

        query = Visibility(node.visibility).get_query(node)

        nodes = self.system_query(query)

        try:
            assert len(nodes) == 1
            assert nodes[0].id == current_user.id
            return True
        except AssertionError:
            return False

    def get_edges_between(self, nodes):

        node_ids = [str(node.id) for node in nodes]

        query = """
        MATCH (a)-[r]->(b)
        WHERE id(a) IN [{ids}] AND id(b) IN [{ids}]
        RETURN r
        """.format(ids=','.join(node_ids))

        return self.query_edges(query)
