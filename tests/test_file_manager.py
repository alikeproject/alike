import file_manager
import nose.tools


def test_get_mimetype():
    # TODO: more tests
    data = {
        'data/img.jpeg': 'image/jpeg',
        'data/img.jpg': 'image/jpeg',
        'data/img.png': 'image/png',
        'data/img.bmp': 'image/x-ms-bmp',
        'data/img.gif': 'image/gif',
        'data/an.html': 'text/html'
        #'data/anapp.exe': 'application/x-msdownload'
    }
    for path, mimetype in data.iteritems():
        result = file_manager.get_mimetype('tests/' + path)
        yield nose.tools.assert_equals, result, mimetype


def test_get_extension():
    data = {
        'file.ext': 'ext',
        'file.a.b': 'b',
        'file.': '',
        '.': ''
    }
    for name, ext in data.iteritems():
        extension = file_manager.get_extension(name)
        yield nose.tools.assert_equals, extension, ext
