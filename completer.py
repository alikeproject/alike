import re
from flask_login import current_user


def escape_cypher_string(cypher_string):
    return cypher_string.replace('\'', '\\\'')


class Dictionary(object):

    def __init__(self):

        # TODO maybye just use SCOWL https://github.com/kevina/wordlist ?
        # TODO: implement a trie to store this words?
        with open('/usr/share/dict/words') as words_file:
            self.words = words_file.read().splitlines()

    def match(self, query, at_most=100):

        results = []

        counter = 0

        for word in self.words:
            if counter > at_most:
                break
            if re.match(query + '.*', word, re.IGNORECASE):
                results.append(word)
                counter += 1

        return results


dictionary = Dictionary()


class Completer(object):

    def __init__(self, node_manager):
        self.nm = node_manager
        self.nlp = self.nm.nlp

    def build_aliases(self):
        pass

    def complete_aliases(self, query):

        self.build_aliases()

        results = []

        for alias in self.aliases:
            if re.match(query + '.*', alias['value'], re.IGNORECASE):
                results.append(alias)

        return results

    def run_cypher_query(self, cypher, query):

        escaped_query = escape_cypher_string(query)
        escaped_query = escaped_query.lower()

        cypher_query = cypher.format(query=escaped_query)

        return self.nm.query(cypher_query)

    def parse_nodes(self, nodes):

        parsed = []

        for node in nodes:
            owner = self.nm.get(node.owner)
            am_i_owner = owner.id == current_user.id
            if am_i_owner:
                owner = 'you'
            else:
                owner = owner.name

            node_dict = {
                'type': node.type,
                'value': node.name,
                'id': '#' + str(node.id),
                'owner': owner,
                'score': 60 if am_i_owner else 50
            }
            parsed.append(node_dict)

        return parsed

    def prob(self, word):

        return self.nlp.prob(word)


class ThoughtCompleter(Completer):

    def __init__(self, node_manager):
        super(ThoughtCompleter, self).__init__(node_manager)

    def build_aliases(self):

        delimiter = "|"

        self.aliases = [
            {
                'value': 'I',
                'id': '#' + current_user.get_id(),
                'type': 'user',
                'score': 100
            },
            {
                'value': 'my',
                'id': '#' + current_user.get_id() + delimiter + "'s",
                'type': 'user',
                'score': 100
            }
        ]

    def complete_from_dictionary(self, query, pick_best=10):

        all_results = []

        words = dictionary.match(query, at_most=300)

        for word in words:
            all_results.append({'value': word, 'score': self.prob(word)})

        best_results = sorted(all_results, key=lambda x: x['score'], reverse=True)[:pick_best]

        # lets add here some reduntant infos
        for i in range(len(best_results)):
            best_results[i].update({'id': best_results[i]['value'], 'type': 'dict'})

        return best_results

    def complete(self, query, present):

        # get all equal queries and up to 10 queries similar to
        cypher = """
        MATCH (n)
        WHERE n.name =~ '(?i){query}.+'
        RETURN n
        LIMIT 10
        UNION
        MATCH (n)
        WHERE n.name = '(?i){query}'
        RETURN n
        """

        nodes = self.run_cypher_query(cypher, query)

        results = self.parse_nodes(nodes)
        results += self.complete_aliases(query)
        results += self.complete_from_dictionary(query)

        return results


class SearchCompleter(Completer):

    def build_aliases(self):
        self.aliases = [
            {
                'value': 'I',
                'id': '#' + current_user.get_id(),
                'type': 'user',
                'score': 100
            }
        ]

    def complete(self, query):

        # get all equal queries and up to 10 queries similar to
        cypher = """
        MATCH (n)
        WHERE n.name =~ '(?i){query}.+' AND NOT n:action
        RETURN n
        LIMIT 10
        UNION
        MATCH (n)
        WHERE n.name = '(?i){query}' AND NOT n:action
        RETURN n
        """

        nodes = self.run_cypher_query(cypher, query)

        cypher = """
        MATCH (n)
        WHERE n.name =~ '(?i){query}.+' AND n:action
        RETURN distinct n
        UNION
        MATCH (n)
        WHERE n.name = '(?i){query}' AND n:action
        RETURN distinct n
        """

        nodes += self.run_cypher_query(cypher, query)

        results = self.parse_nodes(nodes)
        results += self.complete_aliases(query)

        return results
