import re
from natural_language import NaturalLanguageError
from file_manager import upload_file, UploadException, UploadError
from user_status import StatusManager


class UserInputException(Exception):

    def get_message(self):
        return self.args[0]

    def original_exception(self):
        return self.args[1]


class UserInputError(UserInputException):

    pass


class UserInputInterpreter(object):

    def __init__(self, node_manager):
        self.nm = node_manager
        self.add = {
            'picture': self.add_picture,
            'thought': self.add_thought,
            'thing': self.add_thing,
            'action': self.add_action,
        }

    def add_picture(self, request, user_id, visibility):

        picture = request.files['add-picture']
        label, relative_path = upload_file(picture, user_id)
        node = self.nm.add('picture', label, visibility, src=relative_path)

        return [node], []

    def add_thing(self, request, user_id, visibility):

        label = request.form['add-thing']
        node = self.nm.add('thing', label, visibility)

        return [node], []

    def add_action(self, request, user_id, visibility):

        label = request.form['add-action']
        node = self.nm.add('action', label, visibility)

        return [node], []

    def add_thought(self, request, user_id, visibility):

        new_nodes = []
        new_edges = []

        thought = request.form['add-thought']

        thought = thought.split('|')

        readable = []

        for item in thought:
            node_matched = re.match('#(\d+)', item)
            if node_matched:
                node_id = node_matched.group(1)
                # This simple exception increases accuracy and speed of NL parser
                if node_id == user_id:
                    item = 'I'
                else:
                    item = self.nm.get(node_id).name
            readable.append(item)

        print 'thought', thought
        print 'readable', readable

        root_token, tokens, doc = self.nm.nlp.analyze(readable)

        print 'doc', doc

        # to test start
        grouped = []
        buffered = []
        i = 0
        for token in doc:

            label = str(token).strip()
            if label == readable[i]:
                grouped.append(([token], readable[i]))
                i += 1
            else:
                buffered.append(token)
                if ' '.join([str(x).rstrip() for x in buffered]) == readable[i]:
                    grouped.append((buffered, readable[i]))
                    buffered = []
                else:
                    i += 1

        print readable
        print buffered
        # to test end


        map = dict(zip(readable, thought))


        # create an action
        label = str(root_token).strip()
        root = self.nm.add('action', label, visibility)
        new_nodes.append(root)

        queue = [(root_token, root)]

        while queue:
            root_token, root = queue.pop()

            label = str(root_token).strip()

            for token in root_token.children:
                n_type = 'thing'
                label = str(token).strip()

                # get node id or create if doesn't exist or is an action
                if map.get(label, '') == label:

                    if token.head == root_token and token.dep_ == 'nsubj':
                        subject = token
                        print "Linking subject: " + str(subject) + " with action: " + str(root_token)

                    # a, an, the are not interesting as nodes, let's just append the article to its owner.
                    if token.dep_ == 'det':
                        root.set('det', str(token).rstrip())
                        continue

                    node = self.nm.add(n_type, label, visibility)
                    new_nodes.append(node)
                else:
                    node_id = int(map[label][1:])
                    node = self.nm.get(node_id)

                queue.append((token, node))

                print "Linking root with obj: " + label
                edge = self.nm.add_edge(node, root, token.dep_, visibility)
                new_edges.append(edge)

                print token, token.dep, token.dep_

        return new_nodes, new_edges

    def new_nodes(self, request, current_user):

        result = {'success': 0}

        try:

            node_type = request.form['type']
            user_id = current_user.get_id()

            visibility = request.form['visibility']
            nodes, edges = self.add[node_type](request, user_id, visibility)

            result['new_nodes'] = nodes
            result['new_edges'] = edges

            result['success'] = 1
            result['type'] = node_type

            status = StatusManager(self.nm)

            visibility = request.form['visibility']

            status_data = {
                'nodes': nodes,
                'edges': edges,
                'visibility': visibility,
                'action': 'add',
                'obj_type': node_type
            }

            print status_data

            # update status of user
            status.push(status_data)

        except UploadError as e:
            raise UserInputError(e.get_message(), e)

        except UploadException as e:
            raise UserInputException(e.get_message(), e)

        except NaturalLanguageError as e:
            raise UserInputError(e.get_message(), e)

        return result
