function parseJSON(d){return JSON.parse(d)}

var nodes = new vis.DataSet(startingNodes)
var edges = new vis.DataSet(startingEdges)

var Network = (function ()
{
	var container = document.getElementById('network')

	var network

	var data = {
		nodes: nodes,
		edges: edges
	}
	var options = {
		layout: {randomSeed: 8},
		physics: {adaptiveTimestep: false},
		interaction: {hover: true},
		nodes:
			{
				// shadow:{color:'rgba(0,0,0,.1)'}
			},
		edges:
			{
				font: {align: 'middle'},
				arrows: {to: {scaleFactor: 0.5}}
				// shadow:{color:'rgba(0,0,0,.1)'}
			}
	}
	var zoomOptions = {
		scale: 1.0,
		offset: {x: 0, y: 0},
		animation: {
			duration: 600,
			easingFunction: 'easeOutQuad'
		}
	}
	/*
	function onAddNode(event, properties, senderId)
	{
		console.log(properties);
		for(var i; i < properties.items.length; i++)
		{
			node = data.nodes.get(properties.items[i])
			console.log(node);
		}
		//color:
	}
	*/

	function androidBrowserBugWorkaround(ctx)
	{
		// Android Bug: 35474, maybe also 41312
		var canvas = ctx.canvas
		canvas.style.opacity = '0.99'
		setTimeout(function() {canvas.style.opacity = '1'}, 1)
	}

	var publicSpace = {
		init: function()
		{
			// data.nodes.on('add', onAddNode)
			network = new vis.Network(container, data, options)

			// If Samsung android browser is detected
			if (window.navigator && window.navigator.userAgent.indexOf('534.30') > 0)
			{
				$(container).children().children('canvas').parents('*').css('overflow', 'visible')
				network.on('afterDrawing', androidBrowserBugWorkaround)
			}

			return network
		},
		focus: function(id)
		{
			network.focus(id, zoomOptions)
		}
	}
	return publicSpace
})()

var network = Network.init()
