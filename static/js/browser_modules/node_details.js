
var nodeDetails = (function ()
{
	var element = I('node_details')
	var innerElement = I('node_details_inner')
	var contentElement = I('node_details_content')
	var node = dynamicVariable.firstNode

	var thoughtTo
	var toNode = I('thought_to_node')

	function _activate()
	{
		removeClass(I('nd_label_cont'), 'edition-active')
		addClass(element, 'active')
		nodeCreator.deactivate()
		rightPanel.expand()
	}
	function refreshLabel()
	{
		I('nd_label').innerHTML = node.label

		var hashID = '#' + node.id
		var label = node.label
		var data = {
			type: node.type,
			id: hashID,
			value: label,
			doNotRemove: true
		}

		if(node.id == dynamicVariable.currentUserID)
		{
			label = 'yourself'
			data.value = 'I'
		}
		toNode.innerHTML = label

		if(!thoughtTo)
		{
			thoughtTo = I('thought_to').selectize
		}

		thoughtTo.clearOptions()
		thoughtTo.addOption(data)
		thoughtTo.addItem(hashID)
	}

	var publicSpace = {
		init: function()
		{
			refreshLabel()
		},
		activate: _activate,
		deactivate: function()
		{
			removeClass(element, 'active')
			nodeCreator.activate()
			rightPanel.collapse()
		},
		show: function (id, asynchronous)
		{
			if (node.id == id)
			{
				return true
			}
			if (typeof asynchronous === 'undefined')
			{
				asynchronous = true
			}
			addClass(innerElement, 'dim')
			$.ajax(
				{
					url: '/get_node',
					data: {id: id},
					context: this,
					success: this.load,
					async: asynchronous
				}
			)
		},
		load: function(data)
		{
			node = data.node

			refreshLabel()

			contentElement.innerHTML = data.html
			removeClass(innerElement, 'dim')

			if (node.owner == dynamicVariable.currentUserID)
			{
				addClass(I('nd_label_cont'), 'edit-permission')
			}
			else
			{
				removeClass(I('nd_label_cont'), 'edit-permission')
			}

			_activate()

			var state = {
				func: 'nodeDetails.load',
				data: data
			}
			History.pushState(state, node.label, '/node?id=' + node.id)

			if(data.success)
			{
				// it's necessary when function is invoked from history
				network.selectNodes([node.id])
			}
		},
		editLabel: function()
		{
			var input = I('nd_label_input')
			addClass(I('nd_label_cont'), 'edition-active')
			input.value = I('nd_label').innerHTML
			input.focus()
		},
		removeNode: function()
		{
			// TODO confirmation

			var data = {id: node.id}

			$.ajax(
				{
					url: '/remove_node',
					type: 'POST',
					data: JSON.stringify(data),
					contentType: 'application/json',
					context: this,
					success: this.removeNodeSuccess
				}
			)
		},
		removeNodeSuccess: function(data)
		{
			if (data.success)
			{
				nodes.remove(node.id)
				browser.neutralState()
			}
		},
		saveLabel: function()
		{
			var data = $('#nd_label_form').serializeArray()
			data.push({name: 'id', value: node.id})

			$.ajax(
				{
					url: '/change_node_label',
					type: 'POST',
					data: data,
					context: this,
					success: this.saveLabelSuccess
				}
			)
		},
		saveLabelSuccess: function(data)
		{
			// assert node.id == data.id
			node.label = data.label
			refreshLabel()

			removeClass(I('nd_label_cont'), 'edition-active')

			nodes.update({id: node.id, label: node.label})
		}
	}
	return publicSpace
})()

// This piece of code is ugly but works ;)
$('#nd_label_input').on('keyup', function(e)
{
	if (e.keyCode == 27)
	{
		removeClass(I('nd_label_cont'), 'edition-active')
	}
})
