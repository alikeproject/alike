// NOTE: Other type's of request should be added.
$.ajaxSetup({beforeSend: function (xhr, settings)
{
	if (settings.type === 'POST' && !this.crossDomain)
	{
		xhr.setRequestHeader('X-CSRFToken', dynamicVariable.token)
	}
}})
