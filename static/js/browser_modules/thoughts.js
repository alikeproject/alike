var Thoughts = (function ()
{
	var elements = $('.add-thought')
	var previousContent = ''
	var $selectizeList
	var delimiter = '|'

	function format(item, escape)
	{
		// var img = '<img src="/static/icons/"' + (item.img ? item.img : item.type) + '">'

		var x = '<div>' +
				'<span class="name">' + escape(item.value) + '</span>' +
				(item.owner && item.type == 'thing' ? ('<span class="owner">' + escape(item.owner) + '</span>') : '') +
				// img +
			'</div>'
		return x
	}
	function onItemRemove(value, $item)
	{
		var selectize = this.$input[0].selectize

		var options = selectize.options

		if (options[value].doNotRemove)
		{
			selectize.addItem(value)
		}
	}
	function onInputType(content)
	{
		var $selectize = this.$input[0].selectize
		// if user added new character and on the end there is a dot and last time there was no dot at the end.
		if(previousContent.length < content.length && content.slice(-1) == '.' && previousContent.slice(-1) != '.')
		{
			var value = $selectize.getValue()
			var newWord = content.slice(0, -1)

			var options = $selectize.options
			var toAppend = newWord

			var found = false
			for(var option in options)
			{
				// search for entities from database. This is what would be the best to find.
				if(options[option].value == newWord)
				{
					toAppend = options[option].id
					found = true
					break
				}
				// search for entities from dict. Don't break here
				if(options[option].id == newWord)
				{
					found = true
				}
			}
			if(!found)
			{
				$selectize.addOption(createElement(newWord))
			}

			value = value.split(delimiter)
			value.push(toAppend)

			$selectize.setValue(value, true)
			$selectize.blur()
			// move focus to 'submit' button from corresponding form
			$(this.$input[0]).parents('form').find('button[type=submit]').focus()
		}
		if(content.indexOf(delimiter) !== -1)
		{
			console.log(content)
		}
		previousContent = content
	}

	function onProgrammaticChange(event)
	{
		if (event.target.value == '')
		{
			event.target.selectize.clear()
		}
	}

	function createElement(input)
	{
		return {
			value: input,
			id: input
		}
	}

	var publicSpace = {
		init: function()
		{
			$selectizeList = elements.selectize({
				delimiter: delimiter,
				// persist: false,
				create: createElement,
				// createFilter: , TODO
				onType: onInputType,
				onItemRemove: onItemRemove,
				closeAfterSelect: true,
				selectOnTab: true,
				loadThrottle: 150,
				valueField: 'id',
				labelField: 'value',
				optgroupField: 'type',
				optgroupValueField: 'id',
				optgroupLabelField: 'name',
				searchField: 'value',
				optgroups: [
					{id: 'user', name: 'User'},
					{id: 'thing', name: 'Noun'},
					{id: 'action', name: 'Action'},
					{id: 'dict', name: 'Dictionary'}
				],
				render: {
					option: format
				},
				load: function(query, callback) {
					if (!query.length)
					{
						return callback()
					}
					$.ajax({
						url: '/complete',
						type: 'GET',
						data:
							{
								q: encodeURIComponent(query),
								p: encodeURIComponent(this.items)
							},
						error: function()
						{
							callback()
						},
						success: function(res)
						{
							res = JSON.parse(res)
							callback(res)
						}
					})
				}

			})

			// lets enable it permanently - we are switching only disable state in real element,
			// and since the fake does not receive signals about this changes, we leave it always enabled
			// (this also does not affect what will be sent to server)
			for(var i = 0; i < $selectizeList.length; i++)
			{
				$selectizeList[i].selectize.enable()
			}

			// lets propagate changes from original HTML input to selectize by catching and processing programatic changes
			elements.on('change', onProgrammaticChange)
		}
	}

	return publicSpace
})()
