
var newsFeed = (function ()
{
	var minDate
	var maxDate
	var contEl = I('feed_content')
	var moreEl = I('feed_more')
	var counter = 0
	var maxPosts = 10

	function fetchUpdates()
	{
		$.ajax(
			{
				url: '/news_feed',
				data: {date_from: maxDate, max_posts: maxPosts},
				context: this,
				success: addOnTop
			}
		)
	}

	function loadMore()
	{
		$.ajax(
			{
				url: '/news_feed',
				data: {date_to: minDate},
				context: this,
				success: addOnBottom
			}
		)
		return false
	}

	function pack(html, id)
	{
		return '<div id="' + id + '" class="feed_pack new">' + html + '</div>'
	}

	function newID()
	{
		return 'feed_pack_' + counter++
	}

	function addOnTop(data)
	{
		if(data.html)
		{
			maxDate = data.max
			var id = newID()
			contEl.insertAdjacentHTML('afterbegin', pack(data.html, id))
			window.setTimeout(function () {removeClass(I(id), 'new')}, 1)
			window.setTimeout(fetchUpdates, 3500)
		}
		else
		{
			window.setTimeout(fetchUpdates, 2500)
		}
	}

	function addOnBottom(data)
	{
		if(data.html)
		{
			minDate = data.min
			var id = newID()
			contEl.insertAdjacentHTML('beforeend', pack(data.html, id))
			window.setTimeout(function () {removeClass(I(id), 'new')}, 1)
		}
		if(!data.html || data.end)
		{
			moreEl.style.opacity = '0'
		}
	}

	var publicSpace = {
		init: function(min, max)
		{
			minDate = min
			maxDate = max
			window.setTimeout(fetchUpdates, 3000)
			$(moreEl).on('click', loadMore)
		}
	}

	return publicSpace
})()
