var History = (function ()
{
	var preventPushing = false
	var currentState = null

	function defaultTitle(x)
	{
		document.title = x
	}

	var publicSpace = {
		init: function (state, title, address, titleCallback)
		{
			this.titleCallback = (typeof titleCallback === 'undefined') ? defaultTitle : titleCallback
			state.title = title
			currentState = state
			window.history.replaceState(state, title, address)
			window.addEventListener('popstate', this.popState.bind(this))
		},
		pushState: function (state, title, address)
		{
			if (preventPushing)
			{
				console.log('Prevent pushing')
				return false
			}
			state.title = title

			if (JSON.stringify(currentState) === JSON.stringify(state))
			{
				// console.log('Not a change')
				return false
			}

			this.titleCallback(state.title)
			currentState = state

			window.history.pushState(state, title, address)
		},
		popState: function (event)
		{
			var state = event.state
			preventPushing = true
			executeFunctionByName(state.func, window, state.data)
			preventPushing = false
			this.titleCallback(state.title)
			currentState = state
		}
	}
	return publicSpace
})()

/* The function 'executeFunctionByName' was written by StackOverflow user
Jason Bunting, improved by Alex Nazarov and is licensed under CC BY-SA 3.0
http://creativecommons.org/licenses/by-sa/3.0/ There was no modifications
except formatting, the function is excluded from the rest of the Project's code */
function executeFunctionByName(functionName, context /* args */)
{
	var args = Array.prototype.slice.call(arguments, 2)
	var namespaces = functionName.split('.')
	var func = namespaces.pop()
	for (var i = 0; i < namespaces.length; i++)
	{
		context = context[namespaces[i]]
	}
	return context[func].apply(context, args)
}
