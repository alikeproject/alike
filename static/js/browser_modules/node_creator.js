var nodeCreator = (function ()
{
	var element = I('node_creator')
	var currentNodeType
	var progress
	var nodeTypes = ['thing', 'thought', 'action', 'picture']

	var publicSpace = {
		init: function(nodeType, preventExpanding)
		{
			for(var i = 0; i < nodeTypes.length; i++)
			{
				var type = nodeTypes[i]
				if(type != nodeType)
				{
					I('add-' + type).disabled = 'disabled'
					I('add-' + type).value = ''
				}
			}
			currentNodeType = nodeType

			this.setType(nodeType, preventExpanding)

			$('#add-picture').filestyle(
				{
					placeholder: 'No file selected',
					buttonText: 'Browse'
				}
			)
		},
		activate: function()
		{
			addClass(element, 'active')
		},
		deactivate: function()
		{
			removeClass(element, 'active')
		},
		expand: function()
		{
			nodeDetails.deactivate()
			rightPanel.expandPartially()
		},
		collapse: function()
		{
			rightPanel.collapseFromPartially()
		},
		setType: function(type, preventExpanding)
		{
			if(type === 'thought' && !preventExpanding)
			{
				nodeCreator.expand()
			}
			else
			{
				nodeCreator.collapse()
			}

			removeClass(I('st-' + currentNodeType), 'active')
			removeClass(I('add-' + currentNodeType).parentNode, 'shown')
			I('add-' + currentNodeType).disabled = 'disabled'

			currentNodeType = type

			addClass(I('st-' + type), 'active')
			addClass(I('add-' + type).parentNode, 'shown')
			I('add-' + currentNodeType).disabled = ''

			return false
		},
		add: function(altData)
		{
			var data
			if(altData)
			{
				data = altData
			}
			else
			{
				data = new window.FormData(I('add_node_form'))
				data.append('type', currentNodeType)
			}

			// ask server to add node
			$.ajax(
				{
					url: 'add_node',
					context: this,
					type: 'POST',
					xhr: function () {
						return $.ajaxSettings.xhr()
					},
					processData: false,
					contentType: false,
					cache: false,
					data: data,
					success: this.addCallback
				}
			)
		},
		addCallback: function (data)
		{
			if(data.success)
			{
				progress = true
				for(var i = 0; i < data.new_nodes.length; i++)
				{
					var node = data.new_nodes[i]
					var position = network.getViewPosition()
					node.x = position.x
					node.y = position.y
					nodes.add(node)
				}
				for(var i = 0; i < data.new_edges.length; i++)
				{
					edges.add(data.new_edges[i])
				}
				progress = false
				// clear the input
				$('#add-' + data.type).val('').change()
			}
			else
			{
				var error = data.error
				alert(error)
			}
		},
		addingInProgress: function()
		{
			return progress
		}
	}

	return publicSpace
})()

var edgeCreator = (function ()
{
	var awaitingEdges = []

	function onError(xhr, status, error)
	{
		var response = parseJSON(xhr.responseText)
		if(response.vis_id)
		{
			edges.remove(response.vis_id)
			alert('You don\'t have rights to access one of this nodes')
		}
	}

	function onAdd(event, properties)
	{
		if(nodeCreator.addingInProgress())
		{
			return true
		}

		var items = properties.items
		for(var i = 0; i < items.length; i++)
		{
			var edge = edges.get(items[i])

			edge = {
				from: edge.from,
				to: edge.to,
				id: edge.id,
				color: {opacity: 0.3}, dashes: true, label: 'Processing...'}

			edges.update(edge)

			// awaitingEdges.push(edge)

			// ask server to add edge

			var data = {
				from: edge.from,
				to: edge.to,
				vis_id: edge.id
			}

			$.ajax(
				{
					url: 'add_edge',
					context: this,
					contentType: 'application/json',
					type: 'POST',
					data: JSON.stringify(data),
					success: addCallback,
					error: onError
				}
			)
		}
	}

	function addCallback(data)
	{
		edges.update({id: data.vis_id, from: data.from, to: data.to, color: {opacity: 1}, dashes: false, label: ''})
	}

	var publicSpace = {
		init: function()
		{
			edges.on('add', onAdd)
		},
		getAwaitingEdges: function()
		{
			return awaitingEdges
		}
	}
	return publicSpace
})()

edgeCreator.init()
/* {#
network.on("beforeDrawing", function (ctx) {

	var edges = edgeCreator.getAwaitingEdges()
	for (var i = 0; i < edges.length; i++)
	{
		var edge = edges[i]

		var nodePos1 = network.getPositions(edge.from)
		var nodePos2 = network.getPositions(edge.to)

		var posX = ( nodePos1[edge.from].x + nodePos2[edge.to].x ) / 2
		var posY = ( nodePos1[edge.from].y + nodePos2[edge.to].y ) / 2

		ctx.strokeStyle = '#A6D5F7';
		ctx.fillStyle = '#294475';
		ctx.circle(posX, posY, 5);
		ctx.fill();
		ctx.stroke();
	}

  });
#} */
