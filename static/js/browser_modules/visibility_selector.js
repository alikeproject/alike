var VisibilitySelector = (function ()
{
	var preventBubbleSelect = false
	var iconMap = {
		private: 'user',
		public: 'globe',
		custom: 'cog'
	}
	var elements = $('.visibility-select')

	function format(item, escape)
	{
		if(item.value in iconMap)
		{
			var icon = iconMap[item.value]
		}
		else
		{
			var icon = iconMap.custom
		}

		var state = '<div class="visibility-option"><span class="glyphicon glyphicon-' + icon + '" aria-hidden="true"></span>' + item.text + '</div>'

		return state
	}
	function onSelect(value, $item)
	{
		if(preventBubbleSelect)
		{
			return true
		}
		if(value != 'private' && value != 'public')
		{
			var newValue = prompt('New value:')
			if(!newValue)
			{
				return false
			}

			this.updateOption(
				value,
				{
					value: newValue,
					text: 'Custom (' + newValue.substring(0, 6) + '...)'
				}
			)
			preventBubbleSelect = true
			this.setValue(newValue)
			this.refreshOptions(false)
			preventBubbleSelect = false

			this.blur()
		}
	}

	var publicSpace = {
		init: function()
		{
			elements.selectize({
				create: false,
				onChange: onSelect,
				closeAfterSelect: true,
				render: {
					item: format,
					option: format
				}
			})
		}
	}

	return publicSpace
})()
