var SearchBar = (function ()
{
	var elements = $('#viewer-search')

	function format(item, escape)
	{
		// var img = '<img src="/static/icons/"' + (item.img ? item.img : item.type) + '">'

		var html = '<div>' +
		'<span class="name">' + escape(item.value) + '</span>' +
		(item.owner && item.type != 'user' ? ('<span class="owner">' + escape(item.owner) + '</span>') : '') +
		// img +
		'</div>'

		return html
	}
	function onValueChange(value)
	{
		var id = value.match(/#(\d+)/)[1]

		// TODO: fetch node from the server if it is not already loaded (check nodes)
		if(nodeDetails)
		{
			nodeDetails.show(id)
		}
		else
		{
			network.selectNodes([id])
		}
		Network.focus(id)
	}

	var publicSpace = {
		init: function()
		{
			// always clear on load - prevent firefox from messing up
			elements.val('')

			elements.selectize({
				maxItems: 1,
				create: false,
				onChange: onValueChange,
				closeAfterSelect: true,
				valueField: 'id',
				labelField: 'value',
				optgroupField: 'type',
				optgroupValueField: 'id',
				optgroupLabelField: 'name',
				searchField: 'value',
				optgroups: [
					{id: 'user', name: 'User'},
					{id: 'thing', name: 'Noun'},
					{id: 'action', name: 'Action'}
				],
				render: {
					option: format
				},
				load: function(query, callback) {
					if (!query.length)
					{
						return callback()
					}
					$.ajax({
						url: '/search',
						type: 'GET',
						data:
							{
								q: encodeURIComponent(query)
							},
						error: function() {
							callback()
						},
						success: function(rawRsult) {
							var result = JSON.parse(rawRsult)
							callback(result)
						}
					})
				}

			})
		}
	}

	return publicSpace
})()

SearchBar.init()
