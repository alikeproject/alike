var browser = {
	explore: function()
	{
		nodeDetails.deactivate()
		// unselect
		network.selectNodes([])
	},
	node: function(query)
	{
		var node = getParameter('id', query)
		nodeDetails.show(node)
	},
	neutralState: function()
	{
		nodeDetails.deactivate()
		// todo - hardcoded title!

		document.activeElement.blur()

		var state = {
			func: 'browser.explore',
			data: null
		}
		History.pushState(state, 'Explore', '/explore')
	}
}

var rightPanel = {
	element: I('right_panel'),
	expand: function()
	{
		addClass(this.element, 'expanded')
	},
	expandPartially: function()
	{
		addClass(this.element, 'semi-expanded')
	},
	collapse: function()
	{
		removeClass(this.element, 'expanded')
	},
	collapseFromPartially: function()
	{
		removeClass(this.element, 'semi-expanded')
	}
}

function titleCallback(x)
{
	document.title = 'Alike - ' + x
}
History.init({func: 'browser.' + window.location.pathname.substring(1), data: window.location.search}, document.title, window.location.pathname + window.location.search, titleCallback)
VisibilitySelector.init()
Thoughts.init()

network.on('click', function (params)
{
	if(params.edges.length == 1)
	{
		// console.log('Selected edge: ' + params['edges'][0])
	}
	else if (params.nodes.length == 0)
	{
		browser.neutralState()
	}
})

network.on('selectNode', function (params)
{
	if (params.nodes.length == 1)
	{
		nodeDetails.show(params.nodes[0])
	}
})

function goToNode(id, e)
{
	if (!e.ctrlKey)
	{
		nodeDetails.show(id)
		Network.focus(id)
	}
	return e.ctrlKey
}

$(document).on('click', 'a.u', function(e)
{
	var href = e.target.href
	var id = href.match(/\/node\?id=(\d+)/)[1]

	return goToNode(id, e)
})

$('#add-node').click(function()
{
	nodeCreator.add()
	return false
})

$('#add_thought_to_btn').click(function()
{
	var data = new window.FormData(I('add_thought_to'))
	nodeCreator.add(data)
	return false
})

$('#nd_label_edit').click(function()
{
	nodeDetails.editLabel()
	return false
})

$('#nd_remove').click(function()
{
	nodeDetails.removeNode()
	return false
})

$('#nd_label_save').click(function()
{
	nodeDetails.saveLabel()
	return false
})

if (dynamicVariable.firstNode == -1)
{
	nodeCreator.init('thought')
}
else
{
	network.selectNodes([dynamicVariable.firstNode.id])
	nodeCreator.init('thought', true)
}

nodeDetails.init()

newsFeed.init(dynamicVariable.feedMinDate, dynamicVariable.feedMaxDate)
