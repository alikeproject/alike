from datetime import datetime, timedelta


def ago_from_now(**kwargs):
    return datetime.utcnow() - timedelta(**kwargs)


class DateTime(object):

    def __init__(self, date_str):
        self.dt = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S.%f')

    def strftime(self, format):
        return self.dt.strftime(format)

    def to_html(self):
        return self.strftime('%Y-%m-%d %H:%M:%S')

    def _sec_ago(self):
        return (datetime.utcnow() - self.dt).total_seconds()

    @property
    def seconds_ago(self):
        return '{0:.0f}'.format(self._sec_ago())

    @property
    def minutes_ago(self):
        return '{0:.0f}'.format(self._sec_ago() / 60)

    def readable(self):

        if self.dt > ago_from_now(seconds=20):
            return 'just now'

        if self.dt > ago_from_now(minutes=1):
            return '{0} seconds ago'.format(self.seconds_ago)

        if self.dt > ago_from_now(hours=1):
            return '{0} minutes ago'.format(self.minutes_ago)

        if self.dt > ago_from_now(days=1):
            day = 'Today' if self.dt.day == datetime.utcnow().day else 'Yesterday'
            return '{0} {1}'.format(day, self.strftime('%H:%M'))

        if self.dt > ago_from_now(days=5):
            return '{0}'.format(self.strftime('%A %H:%M'))

        return self.strftime('%Y-%m-%d %H:%M')
