import config
import os
import tempfile
import magic
import pyclamd
from shutil import copyfile


class UploadException(Exception):

    def get_message(self):
        return self.args[0].format(*self.args[1:])


class UploadError(UploadException):
    pass


def accepted_mimetypes(accepted_types=None):
    """
    Provides set of mimetypes intended for use in html <input type='file'>
    element, allowed in configuration file and optionally filtered out by
    a set of types (the optional argument). Note, that there 'type' follows
    http://tools.ietf.org/html/rfc6838#section-4.2 convention, so for example
    type of 'image/png' mimetype (or media type) is just 'image'.
    """

    if accepted_types is None:
        # https://www.iana.org/assignments/media-types/media-types.xhtml
        accepted_types = set('application', 'audio', 'example', 'image',
                             'message', 'model', 'multipart', 'text', 'video')

    filtered_types = set()
    for mimetype in config.ALLOWED_MIMETYPES:
        type_name = mimetype.split('/')[0]
        if type_name in accepted_types:
            filtered_types.add(mimetype)

    return filtered_types


def scan_file(path):

    cd = pyclamd.ClamdAgnostic()

    if not cd.ping():
        raise Exception('Antivirus does not ping')

    return cd.scan_file(path)


def get_mimetype(path):
    return magic.from_file(path, mime=True)


def get_extension(filename):
    return '.' in filename and filename.rsplit('.', 1)[1]


def is_allowed_ext(extension):
    return extension.lower() in config.ALLOWED_EXTENSIONS


def is_allowed_mimetype(mimetype):
    return mimetype.lower() in config.ALLOWED_MIMETYPES


def rm_temp_file(temp_file):
    """
    Closes temp_file so it should be removed, and checks if it does not exists.
    """
    # Closing temp file should cause it to be removed automatically.
    temp_file.close()
    # make sure it was removed (slightly vulnerable for false positives)
    if os.path.isfile(temp_file.name):
        msg = 'File of name the same as one inteded to be removed still exists. This may mean serious security issue.'
        raise UploadError(msg, temp_file.name)


def upload_file(file_object, subdirectory):

    # 1. check if the file object was properly passed

    if not file_object:
        raise UploadException('File was not properly recieved')

    # 2. check extension

    extension = get_extension(file_object.filename)
    if not is_allowed_ext(extension):
        raise UploadException('Extension: {0} is not allowed', extension)

    # 3. save file to temp

    temp_file = tempfile.NamedTemporaryFile()
    temp_file.write(file_object.read())

    mimetype = get_mimetype(temp_file.name)
    if not is_allowed_mimetype(mimetype):
        # remove the file
        rm_temp_file(temp_file)
        raise UploadException('Files: {0} are not allowed', mimetype)

    # 4. antiviral scan.

    detected_virus = scan_file(temp_file.name)

    if not detected_virus:
        # remove the file
        rm_temp_file(temp_file)
        raise UploadError('Security thread detected; aborting', detected_virus)

    # 5. if we are still alife, copy the file to new location

    directory = os.path.join(config.UPLOAD_FOLDER, subdirectory)

    if not os.path.isdir(directory):
        os.makedirs(directory)

    file_name, file_extension = os.path.splitext(file_object.filename)

    assert '.' + extension == file_extension

    handle, path = tempfile.mkstemp(dir=directory, prefix='', suffix=file_extension)
    os.close(handle)

    copyfile(temp_file.name, path)

    # 6. and return the data

    relative_path = os.path.relpath(path)

    return file_name, relative_path
