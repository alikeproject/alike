from app import db, node_manager
import security


class AnonymousUser(object):

    def __init__(self):
        """
        Instead of writing thousands of if (user_logged in), I decided to ofer,
        an anonymous user object. It has all capabilities of normal user, and
        simulates behaviour of minimal, hidden user. It is used for example,
        when a guest opens home page, to 'test', wheter a node is visible or not
        (I use an itersecion of user_node and nodes which are allowed to see)
        """
        query = """
        MATCH (n:user)
        WHERE n.name = '_anonymous_user'
        RETURN n;
        """
        tester = node_manager.system_query(query)
        if tester:
            self.id = tester[0].id
        else:
            node = node_manager.add(node_type='user', node_name='_anonymous_user', visibility='private')
            self.id = node.id

    @property
    def is_authenticated(self):
        return False

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return True

    def get_id(self):
        return unicode(self.id)


class User(db.Model):

    id = db.Column('user_id', db.Integer, primary_key=True)
    hash = db.Column('hash', db.String(265))
    # http://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
    email = db.Column('email', db.String(254), unique=True, index=True)

    def __init__(self, password, email):

        initial_username = email.split('@')[0].replace('.', ' ').title()
        # create new node in graph database
        node = node_manager.add(node_type='user', node_name=initial_username, visibility='public')
        # assign id of node from graph database to user record in relational db
        self.id = node.id
        self.hash = security.generate_secret_hash(password)
        self.email = email

    @property
    def is_authenticated(self):
        return True

    @property
    def username(self):
        node = node_manager.get(self.id)
        return node.name

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def authenticate(self, password):
        return security.verify_secret(password, str(self.hash))

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % self.email
