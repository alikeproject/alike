import argparse

if __name__ == "__main__":

    from app import app, node_manager

    parser = argparse.ArgumentParser()
    parser.add_argument('-i',
                        '--host',
                        default=app.config['DEFAULT_HOST'],
                        help='Specify IP address or localhost.')
    parser.add_argument('-p',
                        '--port',
                        default=app.config['DEFAULT_PORT'],
                        type=int)
    parser.add_argument('-d',
                        '--disable_parser',
                        default=False,
                        action='store_true',
                        help='This might be useful for debugging on weaker machines')

    args = parser.parse_args()

    from natural_language import NaturalLanguageParser

    nlp = NaturalLanguageParser()

    node_manager.nlp = nlp

    if not args.disable_parser:
        nlp.load()

    app.run(host=args.host, port=args.port)

else:
    print('This script should be run from command line')
