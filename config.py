SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'
SQLALCHEMY_ECHO = False
SECRET_KEY = '\xfb\x12\xdf\xa1@i\xd6>V\xc0\xbb\x8fp\x16#Z\x0b\x81\xeb\x16'
DEBUG = False
GRAPH_DATABASE = 'http://localhost:7474/db/data/'
GRAPH_USER = 'neo4j'
GRAPH_PASSWORD = 'neo4jneo4j'
UPLOAD_FOLDER = 'static/media/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
DEFAULT_HOST = '0.0.0.0'    # use public IP
DEFAULT_PORT = 5000
ALLOWED_MIMETYPES = {'image/bmp', 'image/png', 'image/gif', 'image/jpeg',
                     'image/x-ms-bmp'}
NODE_TYPES = ['user', 'thought', 'thing', 'action', 'picture', 'status']
MINIFY_PAGE = True
