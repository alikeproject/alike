#import nltk
#from nltk.tokenize import MWETokenizer
#from nltk.internals import find_jars_within_path
#from nltk.parse.stanford import StanfordDependencyParser


class NaturalLanguageError(Exception):
    pass


class NaturalLanguageParser(object):

    def __init__(self):
        self.nlp = None

    def load(self):
        print ' + Loading dependency parser...'
        from spacy.en import English
        self.nlp = English(entity=False, matcher=False)
        print ' + Loading dependency parser finished'

    def nl_to_query(self, sentence):
        # TODO
        return ''
        #return sentence

    def prob(self, word):
        return self.nlp.vocab[unicode(word)].prob

    def analyze(self, tokenized):
        #tokenized = MWETokenizer(sentence)

        #tagged = nltk.pos_tag(tokenized)

        # https://gist.github.com/alvations/e1df0ba227e542955a8a

        #print next(dep_parser.raw_parse("The quick brown fox jumps over the lazy dog."))

        #print [parse.tree() for parse in dep_parser.raw_parse("The quick brown fox jumps over the lazy dog.")]

        sentence = ' '.join(tokenized)

        doc = self.nlp(unicode(sentence))

        tokens = []

        root_token = None
        # 1. find root
        for token in doc:
            if token.head == token:
                root_token = token
            else:
                tokens += [token]

        #sentence = u'I shot an elephant in my sleep'

        #print token

        #result = dependency_parser.raw_parse('I shot an elephant in my sleep')
        #dep = result.next()

        #return list(dep.triples())

        return root_token, tokens, doc
